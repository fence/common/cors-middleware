# CORS Middleware

A simple [PSR-15](https://www.php-fig.org/psr/psr-15/) middleware for handling CORS.

## Installation

Install using Composer:

```bash
composer require glance-project/cors-middleware
```

## Usage

This middleware can be used with any framework compatible with PSR-7 and PSR-15.
On the following examples, Slim will be used.

### Basic usage

On most of the cases, the middleware can be used out of the box.

```php
<?php

use Glance\CorsMiddleware\CorsMiddleware;

$app = new \Slim\App();

$corsMiddleware = CorsMiddleware::create();

$app->add($corsMiddleware);
```

It will add the following headers to your response:

```
Access-Control-Allow-Origin: *
Access-Control-Allow-Headers: Content-Type, Authorization
Access-Control-Allow-Methods: GET, POST, PUT, DELETE, PATCH
```

If the request has the method `OPTIONS` and empty response will be returned.

### Custom origins

```php
$corsMiddleware = CorsMiddleware::create()
    ->withAllowedOrigins(["localhost"]);
```

### Custom headers

```php
$corsMiddleware = CorsMiddleware::create()
    ->withAllowedHeaders(["Content-Type", "Api-Key"]);
```

### Custom methods

```php
$corsMiddleware = CorsMiddleware::create()
    ->withAllowedMethods(["GET", "POST"]);
```