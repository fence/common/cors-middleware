<?php

namespace GlanceProject\CorsMiddleware\Tests\Unit;

use GlanceProject\CorsMiddleware\CorsMiddleware;
use Nyholm\Psr7\Response;
use Nyholm\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Psr\Http\Server\RequestHandlerInterface;

class CorsMiddlewareTest extends TestCase
{
    public function testMiddleware(): void
    {
        $request = new ServerRequest("GET", "http://glance.cern.ch/api/members");

        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->method("handle")->willReturn(new Response());

        $middleware = CorsMiddleware::create();

        $response = $middleware->process($request, $handler);

        $allowedOrigins = $response->getHeader("Access-Control-Allow-Origin");
        $allowedHeaders = $response->getHeader("Access-Control-Allow-Headers");
        $allowedMethods = $response->getHeader("Access-Control-Allow-Methods");

        $this->assertSame([ "*" ], $allowedOrigins);
        $this->assertSame([ "Content-Type, Authorization" ], $allowedHeaders);
        $this->assertSame([ "GET, POST, PUT, DELETE, PATCH" ], $allowedMethods);
    }

    public function testOptionsRequest(): void
    {
        $request = new ServerRequest("OPTIONS", "http://glance.cern.ch/api/members");

        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->method("handle")->willReturn(new Response(400));

        $middleware = CorsMiddleware::create();

        $response = $middleware->process($request, $handler);

        $allowedOrigins = $response->getHeader("Access-Control-Allow-Origin");
        $allowedHeaders = $response->getHeader("Access-Control-Allow-Headers");
        $allowedMethods = $response->getHeader("Access-Control-Allow-Methods");

        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame([ "*" ], $allowedOrigins);
        $this->assertSame([ "Content-Type, Authorization" ], $allowedHeaders);
        $this->assertSame([ "GET, POST, PUT, DELETE, PATCH" ], $allowedMethods);
    }

    public function testCustomAllowedOrigins(): void
    {
        $request = new ServerRequest("GET", "http://glance.cern.ch/api/members");

        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->method("handle")->willReturn(new Response(200));

        $customOrigins = [ "http://localhost:8080", "http://glance.cern.ch/client" ];
        $middleware = CorsMiddleware::create()
                        ->withAllowedOrigins($customOrigins);

        $response = $middleware->process($request, $handler);

        $allowedOrigins = $response->getHeader("Access-Control-Allow-Origin");

        $this->assertSame([ "http://localhost:8080, http://glance.cern.ch/client" ], $allowedOrigins);
    }

    public function testCustomAllowedHeaders(): void
    {
        $request = new ServerRequest("GET", "http://glance.cern.ch/api/members");

        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->method("handle")->willReturn(new Response(200));

        $customHeaders = [ "Content-Type", "Api-Key" ];
        $middleware = CorsMiddleware::create()
                        ->withAllowedHeaders($customHeaders);

        $response = $middleware->process($request, $handler);

        $allowedHeaders = $response->getHeader("Access-Control-Allow-Headers");

        $this->assertSame([ "Content-Type, Api-Key" ], $allowedHeaders);
    }

    public function testCustomAllowedMethods(): void
    {
        $request = new ServerRequest("GET", "http://glance.cern.ch/api/members");

        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->method("handle")->willReturn(new Response(200));

        $customMethods = [ "GET", "POST" ];
        $middleware = CorsMiddleware::create()
                        ->withAllowedMethods($customMethods);

        $response = $middleware->process($request, $handler);

        $allowedMethods = $response->getHeader("Access-Control-Allow-Methods");

        $this->assertSame([ "GET, POST" ], $allowedMethods);
    }

    public function testCustomBypassMethods(): void
    {
        $request = new ServerRequest("MYOPTIONS", "http://glance.cern.ch/api/members");

        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->method("handle")->willReturn(new Response(400));

        $customMethods = [ "MYOPTIONS" ];
        $middleware = CorsMiddleware::create()
                        ->withBypassMethods($customMethods);

        $response = $middleware->process($request, $handler);

        $this->assertSame(200, $response->getStatusCode());
    }
}
