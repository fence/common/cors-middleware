<?php

namespace GlanceProject\CorsMiddleware;

use Http\Discovery\Psr17FactoryDiscovery;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class CorsMiddleware implements MiddlewareInterface
{
    private $responseFactory;
    private $allowedOrigins;
    private $allowedHeaders;
    private $allowedMethods;
    private $bypassMethods;

    /**
     * @param ResponseFactoryInterface $responseFactory
     * @param string[] $allowedOrigins
     * @param string[] $allowedHeaders
     * @param string[] $allowedMethods
     * @param string[] $bypassMethods
     */
    private function __construct(
        ResponseFactoryInterface $responseFactory,
        array $allowedOrigins,
        array $allowedHeaders,
        array $allowedMethods,
        array $bypassMethods
    ) {
        $this->responseFactory = $responseFactory;
        $this->allowedOrigins = $allowedOrigins;
        $this->allowedHeaders = $allowedHeaders;
        $this->allowedMethods = $allowedMethods;
        $this->bypassMethods = $bypassMethods;
    }

    public static function create(): self
    {
        return new self(
            Psr17FactoryDiscovery::findResponseFactory(),
            ["*"],
            ["Content-Type", "Authorization"],
            ["GET", "POST", "PUT", "DELETE", "PATCH"],
            ["OPTIONS"]
        );
    }

    /**
     * Replace allowed origins
     *
     * Default value: "*"
     *
     * @param string[] $allowedOrigins
     *
     * @return self
     */
    public function withAllowedOrigins(array $allowedOrigins): self
    {
        return new self(
            $this->responseFactory,
            $allowedOrigins,
            $this->allowedHeaders,
            $this->allowedMethods,
            $this->bypassMethods
        );
    }

    /**
     * Replace allowed headers
     *
     * Default value: "Content-Type, Authorization"
     *
     * @param string[] $allowedHeaders
     *
     * @return self
     */
    public function withAllowedHeaders(array $allowedHeaders): self
    {
        return new self(
            $this->responseFactory,
            $this->allowedOrigins,
            $allowedHeaders,
            $this->allowedMethods,
            $this->bypassMethods
        );
    }

    /**
     * Replace allowed methods
     *
     * Default value: "GET, POST, PUT, DELTE, PATCH"
     *
     * @param string[] $allowedMethods
     *
     * @return self
     */
    public function withAllowedMethods(array $allowedMethods): self
    {
        return new self(
            $this->responseFactory,
            $this->allowedOrigins,
            $this->allowedHeaders,
            $allowedMethods,
            $this->bypassMethods
        );
    }

    /**
     * Replace bypass methods.
     *
     * Default value: "OPTIONS"
     *
     * @param string[] $bypassMethods
     *
     * @return self
     */
    public function withBypassMethods(array $bypassMethods): self
    {
        return new self(
            $this->responseFactory,
            $this->allowedOrigins,
            $this->allowedHeaders,
            $this->allowedMethods,
            $bypassMethods
        );
    }

    public function process(Request $request, RequestHandlerInterface $handler): Response
    {
        $response = $this->responseFactory->createResponse();

        $bypass = in_array($request->getMethod(), $this->bypassMethods);
        if (!$bypass) {
            $response = $handler->handle($request);
        }

        return $response
            ->withHeader("Access-Control-Allow-Origin", implode(", ", $this->allowedOrigins))
            ->withHeader("Access-Control-Allow-Headers", implode(", ", $this->allowedHeaders))
            ->withHeader("Access-Control-Allow-Methods", implode(", ", $this->allowedMethods));
    }
}
